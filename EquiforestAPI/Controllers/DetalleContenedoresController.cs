﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EquiforestAPI.Models;
using EquiforestWebApp.Models;

namespace EquiforestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Comercializacion/DetalleContenedores")]
    public class DetalleContenedoresController : Controller
    {
        private readonly EquiforestDBContext _context;

        public DetalleContenedoresController(EquiforestDBContext context)
        {
            _context = context;
        }

        // GET: api/DetalleContenedores
        [HttpGet]
        public IEnumerable<DetalleContenedores> GetDetalleContenedores()
        {
            return _context.DetalleContenedores;
        }

        // GET: api/DetalleContenedores/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetalleContenedores([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var detalleContenedores = await _context.DetalleContenedores.SingleOrDefaultAsync(m => m.Id == id);

            if (detalleContenedores == null)
            {
                return NotFound();
            }

            return Ok(detalleContenedores);
        }

        // PUT: api/DetalleContenedores/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetalleContenedores([FromRoute] int id, [FromBody] DetalleContenedores detalleContenedores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detalleContenedores.Id)
            {
                return BadRequest();
            }

            _context.Entry(detalleContenedores).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleContenedoresExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DetalleContenedores
        [HttpPost]
        public async Task<IActionResult> PostDetalleContenedores([FromBody] [Bind("Packing_list" , "Circunferencia" , "Longitud")] IEnumerable<DetalleContenedores> detalleContenedores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.DetalleContenedores.AddRange(detalleContenedores);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                //if (ViajesExists(viajes.Id))
                //{
                //    return new StatusCodeResult(StatusCodes.Status409Conflict);
                //}
                //else
                //{
                //    throw;
                //}
            }

            return CreatedAtAction("GetDetalleContenedores", new { id = 0 }, detalleContenedores);
        }

        // DELETE: api/DetalleContenedores/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDetalleContenedores([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var detalleContenedores = await _context.DetalleContenedores.SingleOrDefaultAsync(m => m.Id == id);
            if (detalleContenedores == null)
            {
                return NotFound();
            }

            _context.DetalleContenedores.Remove(detalleContenedores);
            await _context.SaveChangesAsync();

            return Ok(detalleContenedores);
        }

        private bool DetalleContenedoresExists(int id)
        {
            return _context.DetalleContenedores.Any(e => e.Id == id);
        }
    }
}