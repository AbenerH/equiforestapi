﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EquiforestAPI.Models;
using EquiforestAPI.Models.Cosechas;

namespace EquiforestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Cosechas/Rodales")]
    public class RodalesController : Controller
    {
        private readonly EquiforestDBContext _context;

        public RodalesController(EquiforestDBContext context)
        {
            _context = context;
        }

        // GET: api/Rodales
        [HttpGet]
        public IEnumerable<Rodales> GetRodales()
        {
            return _context.Rodales;
        }

        // GET: api/Rodales/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRodales([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var rodales = await _context.Rodales.SingleOrDefaultAsync(m => m.Id == id);

            if (rodales == null)
            {
                return NotFound();
            }

            return Ok(rodales);
        }

        // PUT: api/Rodales/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRodales([FromRoute] int id, [FromBody] Rodales rodales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rodales.Id)
            {
                return BadRequest();
            }

            _context.Entry(rodales).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RodalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Rodales
        [HttpPost]
        public async Task<IActionResult> PostRodales([FromBody] Rodales rodales)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Rodales.Add(rodales);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRodales", new { id = rodales.Id }, rodales);
        }

        // DELETE: api/Rodales/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRodales([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var rodales = await _context.Rodales.SingleOrDefaultAsync(m => m.Id == id);
            if (rodales == null)
            {
                return NotFound();
            }

            _context.Rodales.Remove(rodales);
            await _context.SaveChangesAsync();

            return Ok(rodales);
        }

        private bool RodalesExists(int id)
        {
            return _context.Rodales.Any(e => e.Id == id);
        }
    }
}