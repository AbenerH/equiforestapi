﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EquiforestAPI.Models;

namespace EquiforestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Cosechas/Trozas")]
    public class TrozasController : Controller
    {
        private readonly EquiforestDBContext _context;

        public TrozasController(EquiforestDBContext context)
        {
            _context = context;
        }

        // GET: api/Trozas
        [HttpGet]
        public IEnumerable<Trozas> GetTrozas()
        {
            return _context.Trozas;
        }

        // GET: api/Trozas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrozas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trozas = await _context.Trozas.SingleOrDefaultAsync(m => m.Id == id);

            if (trozas == null)
            {
                return NotFound();
            }

            return Ok(trozas);
        }

        // PUT: api/Trozas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrozas([FromRoute] int id, [FromBody] Trozas trozas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trozas.Id)
            {
                return BadRequest();
            }

            _context.Entry(trozas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrozasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Trozas
        [HttpPost]
        public async Task<IActionResult> PostTrozas([FromBody] IEnumerable<Trozas> trozas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Trozas.AddRange(trozas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTrozas", new { id = 0 }, trozas);
        }

        // DELETE: api/Trozas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrozas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trozas = await _context.Trozas.SingleOrDefaultAsync(m => m.Id == id);
            if (trozas == null)
            {
                return NotFound();
            }

            _context.Trozas.Remove(trozas);
            await _context.SaveChangesAsync();

            return Ok(trozas);
        }

        private bool TrozasExists(int id)
        {
            return _context.Trozas.Any(e => e.Id == id);
        }
    }
}