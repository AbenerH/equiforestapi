﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EquiforestAPI.Models;
using EquiforestWebApp.Models;
using Microsoft.AspNetCore.JsonPatch;
using EquiforestAPI.Models.Comercializacion;
using Newtonsoft.Json;

namespace EquiforestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Comercializacion/Contenedores")]
    public class ContenedoresController : Controller
    {
        private readonly EquiforestDBContext _context;
        private readonly PackingListController _packingListController;

        public ContenedoresController(EquiforestDBContext context)
        {
            _context = context;
            _packingListController = new PackingListController(context);
        }

        // GET: api/Contenedores
        [HttpGet]
        public IEnumerable<Contenedores> GetContenedores()
        {
            return _context.Contenedores;
        }

        // GET: api/Contenedores/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContenedores([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var contenedores = await _context.Contenedores.SingleOrDefaultAsync(m => m.Id == id);

            if (contenedores == null)
            {
                return NotFound();
            }

            return Ok(contenedores);
        }

        // PUT: api/Contenedores/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContenedores([FromRoute] int id, [FromBody] Contenedores contenedores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contenedores.Id)
            {
                return BadRequest();
            }

            _context.Entry(contenedores).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContenedoresExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Contenedores
        [HttpPost]
        public async Task<IActionResult> PostContenedores([FromBody] [Bind("Contenedor", "Cabezal", "Chofer", "Sello", "FechaSalida", "MaxCargoWGT", "FacturaNo")] IEnumerable<Contenedores> contenedores)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (contenedores.Any())
            {
                _context.Contenedores.AddRange(contenedores);

                try
                {
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateException e)
                {
                    //if (ViajesExists(viajes.Id))
                    //{
                    //    return new StatusCodeResult(StatusCodes.Status409Conflict);
                    //}
                    //else
                    //{
                    //    throw;
                    //}
                }

                foreach (var container in contenedores)
                {
                    var result = _context.PackingList.SingleOrDefault(m => m.PackingListId == container.PackingListId);

                    if (result != null)
                    {
                        if (result.ContenedorId == null)
                        {
                            await updatePackingContainer(container.ContenedorId, result);
                        }
                    }

                }
            }

            return CreatedAtAction("GetContenedores", new { id = 0 }, contenedores);
        }

        private async Task<IActionResult> updatePackingContainer(string contenedorId, Models.Comercializacion.PackingList result)
        {
            JsonPatchDocument<PackingList> patchDoc = new JsonPatchDocument<PackingList>();
            patchDoc.Replace(e => e.ContenedorId, contenedorId);

            return await _packingListController.PatchPackingListAsync(result.PackingListId, patchDoc);

        }


        // DELETE: api/Contenedores/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContenedores([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var contenedores = await _context.Contenedores.SingleOrDefaultAsync(m => m.Id == id);
            if (contenedores == null)
            {
                return NotFound();
            }

            _context.Contenedores.Remove(contenedores);
            await _context.SaveChangesAsync();

            return Ok(contenedores);
        }

        private bool ContenedoresExists(int id)
        {
            return _context.Contenedores.Any(e => e.Id == id);
        }
    }
}