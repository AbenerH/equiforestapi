﻿using EquiforestAPI.Models;
using EquiforestAPI.Models.Comercializacion;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace EquiforestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Comercializacion/PackingList")]
    public class PackingListController : Controller
    {
        private readonly EquiforestDBContext _context;

        public PackingListController(EquiforestDBContext context)
        {
            _context = context;
        }

        // GET: api/PackingList
        [HttpGet]
        public IEnumerable<PackingList> GetPackingLists()
        {
            return _context.PackingList;
        }

        // GET: api/PackingList/EncodedURL
        //[HttpGet("{packingListId}")]
        //public async Task<IActionResult> GetPackingList([FromRoute] string packingListId)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var packingList = await _context.PackingList.SingleOrDefaultAsync(m => m.PackingListId == packingListId);

        //    if (packingList == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(packingList);
        //}

        // GET: api/PackingList/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPackingList([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var packingList = await _context.Contenedores.SingleOrDefaultAsync(m => m.Id == id);

            if (packingList == null)
            {
                return NotFound();
            }

            return Ok(packingList);
        }

        // PUT: api/PackingList/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPackingList([FromRoute] int id, [FromBody] PackingList packingList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != packingList.Id)
            {
                return BadRequest();
            }

            _context.Entry(packingList).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PackingListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PATCH: api/PackingList/5
        [HttpPatch("{Id}")]
        public async Task<IActionResult> PatchPackingListAsync([FromRoute] string id, [FromBody] JsonPatchDocument<PackingList> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            //In your application get Object review object from database base on review Id
            var packingListFromDb = await _context.PackingList.SingleOrDefaultAsync(m => m.PackingListId == id);

            if (packingListFromDb == null)
            {
                return NotFound();
            }

            //Apply changes to Object
            patchDoc.ApplyTo(packingListFromDb);

            // Say to entity framework that you have changes in book entity and it's modified
            _context.Entry(packingListFromDb).State = EntityState.Modified;

            // Save changes to database
            await _context.SaveChangesAsync();

            //update Object in database and return it updated 
            return Ok(packingListFromDb);

        }

        // POST: api/PackingList
        [HttpPost]
        public async Task<IActionResult> PostPackingList([FromBody] IEnumerable<PackingList> packingLists)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<PackingList> list = new List<PackingList>();
            List<PackingList> delete_list = new List<PackingList>();

            list = packingLists.ToList();
            int size = list.Count();

            //Busqueda
            if (size > 0)
            {
                for (int i = 0; i < size; i++)
                { 
                    var checkPacking = _context.PackingList.SingleOrDefault(p => p.PackingListId == list[i].PackingListId);

                    //Insercion
                    if(checkPacking != null)
                    {
                        delete_list.Add(checkPacking);
                    }
                }

                //Eliminacion
                if(delete_list.Count() > 0)
                {
                    list.RemoveAll(x => delete_list.Any(y => y.PackingListId == x.PackingListId));
                }

                if(list.Count() > 0)
                {
                    try
                    {
                        _context.PackingList.AddRange(list);
                        await _context.SaveChangesAsync();

                    }
                    catch (DbUpdateException ex)
                    {
                        return StatusCode(409);

                    }
                }
            }

            return CreatedAtAction("GetPackingList", new { id = 0 }, list);
        }

        // DELETE: api/PackingList/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePackingList([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var packingList = await _context.PackingList.SingleOrDefaultAsync(m => m.Id == id);
            if (packingList == null)
            {
                return NotFound();
            }

            _context.PackingList.Remove(packingList);
            await _context.SaveChangesAsync();

            return Ok(packingList);
        }

        private bool PackingListExists(int id)
        {
            return _context.PackingList.Any(e => e.Id == id);
        }
    }

}