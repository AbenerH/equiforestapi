﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EquiforestAPI.Models;

namespace EquiforestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Cosechas/Viajes")]
    public class ViajesController : Controller
    {
        private readonly EquiforestDBContext _context;

        public ViajesController(EquiforestDBContext context)
        {
            _context = context;
        }

        // GET: api/Viajes
        [HttpGet]
        public IEnumerable<Viajes> GetViajes()
        {
            return _context.Viajes;
        }

        // GET: api/Viajes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetViajes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var viajes = await _context.Viajes.SingleOrDefaultAsync(m => m.Id == id);

            if (viajes == null)
            {
                return NotFound();
            }

            return Ok(viajes);
        }

        // PUT: api/Viajes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutViajes([FromRoute] int id, [FromBody] Viajes viajes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viajes.Id)
            {
                return BadRequest();
            }

            _context.Entry(viajes).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViajesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        public string Nucleo { get; set; }
        public string Finca { get; set; }
        public string Rodal { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public string Ubicacion { get; set; }
        public int? N_Viaje { get; set; }

        // POST: api/Viajes
        [HttpPost]
        public async Task<IActionResult> PostViajes([FromBody] [Bind("Nucleo","Finca","Rodal","FechaIngreso","Ubicacion","N_Viaje", "NoTrozasC")] IEnumerable<Viajes> viajes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Viajes.AddRange(viajes);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                //if (ViajesExists(viajes.Id))
                //{
                //    return new StatusCodeResult(StatusCodes.Status409Conflict);
                //}
                //else
                //{
                //    throw;
                //}
            }

            return CreatedAtAction("GetViajes", new { id = 0 }, viajes);

        }

        // DELETE: api/Viajes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteViajes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var viajes = await _context.Viajes.SingleOrDefaultAsync(m => m.Id == id);
            if (viajes == null)
            {
                return NotFound();
            }

            _context.Viajes.Remove(viajes);
            await _context.SaveChangesAsync();

            return Ok(viajes);
        }

        private bool ViajesExists(int id)
        {
            return _context.Viajes.Any(e => e.Id == id);
        }
    }
}