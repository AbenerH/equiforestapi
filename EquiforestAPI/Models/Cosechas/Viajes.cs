﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace EquiforestAPI.Models
{
    public partial class Viajes
    {

        [BindNever]
        public int Id { get; set; }
        public string Nucleo { get; set; }
        public string Finca { get; set; }
        public string Rodal { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public string Ubicacion { get; set; }
        public int? N_Viaje { get; set; }
        public string ViajeId { get; set; }
        public int? NoTrozasC { get; set; }

    }
}
