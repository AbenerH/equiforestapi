﻿using System;
using System.Collections.Generic;

namespace EquiforestAPI.Models
{
    public partial class Trozas
    {
        public int Id { get; set; }
        public string Clasificacion { get; set; }
        public float? Circunferencia { get; set; }
        public float? Largo { get; set; }
        public string ViajeId { get; set; }
    }
}
