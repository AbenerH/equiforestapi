﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestAPI.Models.Cosechas
{
    public class Rodales
    {
        public int Id { get; set; }
        public string Finca { get; set; }
        public string Nucleo { get; set; }
        public string Rodal { get; set; }

    }
}
