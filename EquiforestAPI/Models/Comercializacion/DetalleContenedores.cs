﻿using EquiforestAPI.Models.Comercializacion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class DetalleContenedores
    {
        public int Id { get; set; }
        public float Circunferencia { get; set; }
        public float Longitud { get; set; }
        [Required, MaxLength(30)]
        public string PackingListId { get; set; }
    }
}
