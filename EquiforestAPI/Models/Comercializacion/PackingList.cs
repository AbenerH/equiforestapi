﻿using EquiforestWebApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestAPI.Models.Comercializacion
{
    public class PackingList
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int Consecutivo { get; set; }
        public string Clase { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string PackingListId { get; set; }
        public string Ubicacion { get; set; }
        public float DescuentoCrc { get; set; }
        public float DescuentoLong { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string ContenedorId { get; set; }

    }
}
