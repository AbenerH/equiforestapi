﻿using System;
using EquiforestWebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using EquiforestAPI.Models.Cosechas;
using EquiforestAPI.Models.Comercializacion;

namespace EquiforestAPI.Models
{
    public partial class EquiforestDBContext : DbContext
    {
        public DbSet<Trozas> Trozas { get; set; }
        public DbSet<Viajes> Viajes { get; set; }
        public DbSet<Facturas> Facturas { get; set; }
        public DbSet<Contenedores> Contenedores { get; set; }
        public DbSet<DetalleContenedores> DetalleContenedores { get; set;}
        public DbSet<Rodales> Rodales { get; set; }
        public DbSet<PackingList> PackingList { get; set; }

        public EquiforestDBContext(DbContextOptions<EquiforestDBContext> options)
            : base(options)
        {
        }
    }
}
